#!/bin/bash
########################################################################
SCRIPT_PATH="${BASH_SOURCE[0]}"
if ([ -h "${SCRIPT_PATH}" ]) then
	while([ -h "${SCRIPT_PATH}" ]) do
		SCRIPT_PATH=`readlink "${SCRIPT_PATH}"` 
	done
fi
pushd . > /dev/null
cd `dirname ${SCRIPT_PATH}` > /dev/null
SCRIPT_PATH=`pwd`;
popd  > /dev/null
########################################################################
. ${SCRIPT_PATH}/bootloaderlib.sh

check_root $0

SDCARD_IMG="sdcard.img"
SDCARD_CONTENT=boot
SDCARD_PARTNUM=1

# Get SDCard image info

if [ -f ${SDCARD_IMG} ]; then
	# Regular file sdcard image
	sector_size=$( ${FDISK} -lu $SDCARD_IMG | grep "Units" | sed -e 's/.*=//;s/ bytes//')
	start_sector=$( ${FDISK} -lu $SDCARD_IMG | grep "${SDCARD_IMG}${SDCARD_PARTNUM}" | awk '{print $2}' )
	have_partition=$( ${FDISK} -lu $SDCARD_IMG | grep "${SDCARD_IMG}${SDCARD_PARTNUM}" )

	if [ -z "${have_partition}" ]; then
		echo "Error: sdcard does not have partition #${SDCARD_PARTNUM}"
		exit 1
	fi

	LODEV=$(get_loopdev)
	MNT=$(mktemp -d)

	losetup $LODEV $SDCARD_IMG -o $(($start_sector * $sector_size))
elif [ -b ${SDCARD_IMG} ]; then
	# Block device, real hw
	LODEV=${SDCARD_IMG}${SDCARD_PARTNUM}
	MNT=$(mktemp -d)
	
	# umount all SDCARD_IMG partitions
	for p in ls ${SDCARD_IMG}*; do
		umount -l ${p} &> /dev/null
	done
else
	echo "Error: unsupported sdcard image type"
	exit 1
fi

mount $LODEV $MNT

tree $MNT/bundles

umount $MNT
rm -rf $MNT

if [ -f ${SDCARD_IMG} ]; then
	losetup -d $LODEV
fi